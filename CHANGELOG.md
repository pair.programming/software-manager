# Changelog

- 02 Feb, 2021 05:05
  - Added IO lib

- 02 Feb, 2021 11:00
  - Changed filename convention to '.hh' for header files
  - Changed Executable name to `sman` from `software-manager`

- 02 Feb, 2021 13:39
  - Added Main Menu UI lib
  - Formatted `CMakeLists.txt`

- 04 Feb, 2021 4:19
  - Changed Contributing rules
  - Added Commit instructions
  - Created a separate lib for Main Menu UI Element

- 04 Feb, 2021 7:20
  - Connected to MySQL database;
  - Created DB API lib
  - Boilerplate for ORM

- 04 Feb, 2021 9:36
  - Added Setup Script for Prerequisite packages and SQL;
