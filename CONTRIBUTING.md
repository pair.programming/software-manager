# Contributing Rules

- Conventions
    - Use *snake_case* for variable names
    - Use *snake_case* for library names
    - Use *PascalCase* for lib folder names
    - Use *PascalCase* for class names
    - Use *SCREAMING_SNAKE_CASE* for constants, definitions etc.

- Features
    - To add a feature to the package
        - create a new lib in the *lib* folder with appropriate Source and Header files
        - reference it in the CMakeLists.txt file
            `add_library(lib_name ...lib_sources)`
            `target_include_directories(lib_name ...required_header_files)`

        - link lib to the Executable
            `target_link_libraries(sman lib_name)`
            `target_include_directories(sman PRIVATE lib/lib_directory)`

    - Make sure to mention your changes in the CHANGELOG.md file **before** making a commit.
        - The format for the CHANGELOG is as follows
            ```- <Date (DD, Month, YYYY)> <Time (HH:MM) GMT +0)>
                   - Changes
                   - that
                   - You
                   - Make       
            ```


- Push changes to remote repo.
  - Write your code in the `dev` branch
  - Create a merge request to the `master` branch

- Issues
  - Create an issue on the respective feature on Gitlab on the respective branch.
