#include <fmt/format.h>
#include <sqlite3.h>

#include <iostream>

using namespace std;

class DBManager {
 private:
  sqlite3* maindb;
  char* zErrMsg = 0;
  int resp;

  bool error_handler(int resp) {
    if (resp != SQLITE_OK) {
      return false;
    } else {
      return true;
    }
  }

  static int callback(void* data, int argc, char** argv, char** azColName) {
    int i;
    fprintf(stderr, "%s: ", (const char*)data);
    for (i = 0; i < argc; i++) {
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
  }

 public:
  DBManager() {
    resp = sqlite3_open("database.db", &maindb);
    if (resp != SQLITE_OK) {
      cout << "error: " << sqlite3_errmsg(maindb) << endl;
    }
  }

  void close() { sqlite3_close(maindb); }

  // ###### Project Table ###### //
  int drop_project_table() {
    const char* sql;
    sql = "DROP TABLE projectlist;";
    resp = sqlite3_exec(maindb, sql, callback, 0, &zErrMsg);
    return error_handler(resp);
  }

  int create_project_table() {
    const char* sql;
    sql =
        "CREATE TABLE IF NOT EXISTS projectlist("
        "id             INTEGER PRIMARY KEY AUTOINCREMENT,"
        "title          NTEXT    NOT NULL,"
        "todo_id        INTEGER"
        "description    NTEXT,"
        "version        TEXT,"
        "status         INTEGER,"
        "FOREIGN KEY (todo_id) REFERENCES todolist(id) ON DELETE CASCADE"
        ");";
    resp = sqlite3_exec(maindb, sql, callback, 0, &zErrMsg);
    return error_handler(resp);
  }

  int insert_project_data(string title, string description, string version) {
    const char* sql;
    string dyn_string =
        "INSERT INTO projectlist (title,description,version,status) "
        "VALUES ('{0}', '{1}', '{2}', 0); ";
    dyn_string = fmt::format(dyn_string, title, description, version);
    sql = dyn_string.c_str();
    resp = sqlite3_exec(maindb, sql, callback, 0, &zErrMsg);
    return error_handler(resp);
  }

  int update_project_data(int id) {
    const char* sql;
    string dyn_string = "UPDATE projectlist SET status=1 WHERE id = {0} ;";
    dyn_string = fmt::format(dyn_string, id);
    sql = dyn_string.c_str();
    resp = sqlite3_exec(maindb, sql, callback, 0, &zErrMsg);
    return error_handler(resp);
  }

  int delete_project_data(int id) {
    const char* sql;
    string dyn_string = "DELETE FROM projectlist WHERE id = {0} ;";
    dyn_string = fmt::format(dyn_string, id);
    sql = dyn_string.c_str();
    resp = sqlite3_exec(maindb, sql, callback, 0, &zErrMsg);
    return error_handler(resp);
  }

  // ###### Todo Table ###### //
  int drop_todo_table() {
    const char* sql;
    sql = "DROP TABLE projectlist;";
    resp = sqlite3_exec(maindb, sql, callback, 0, &zErrMsg);
    return error_handler(resp);
  }

  int create_todo_table() {
    const char* sql;
    sql =
        "CREATE TABLE IF NOT EXISTS todolist("
        "id             INTEGER PRIMARY KEY AUTOINCREMENT,"
        "project_id     INTEGER"
        "title          NTEXT    NOT NULL,"
        "description    NTEXT,"
        "status         INTEGER,"
        "FOREIGN KEY (project_id) REFERENCES project(id) ON DELETE CASCADE"
        ");";
    resp = sqlite3_exec(maindb, sql, callback, 0, &zErrMsg);
    return error_handler(resp);
  }

  int insert_todo_data(string title, string description) {
    const char* sql;
    string dyn_string =
        "INSERT INTO todolist (title,description,status) "
        "VALUES ('{0}', '{1}', 0); ";
    dyn_string = fmt::format(dyn_string, title, description);
    sql = dyn_string.c_str();
    resp = sqlite3_exec(maindb, sql, callback, 0, &zErrMsg);
    return error_handler(resp);
  }

  int update_todo_data(int id) {
    const char* sql;
    string dyn_string = "UPDATE todolist SET status=1 WHERE id = {0} ;";
    dyn_string = fmt::format(dyn_string, id);
    sql = dyn_string.c_str();
    resp = sqlite3_exec(maindb, sql, callback, 0, &zErrMsg);
    return error_handler(resp);
  }

  int delete_todo_data(int id) {
    const char* sql;
    string dyn_string = "DELETE FROM todolist WHERE id = {0} ;";
    dyn_string = fmt::format(dyn_string, id);
    sql = dyn_string.c_str();
    resp = sqlite3_exec(maindb, sql, callback, 0, &zErrMsg);
    return error_handler(resp);
  }
};