#include <chrono>
#include <thread>

#include "ftxui/component/container.hpp"
#include "ftxui/component/menu.hpp"
#include "ftxui/component/screen_interactive.hpp"
#include "ftxui/screen/string.hpp"

using namespace ftxui;

class MainMenu : public Component {
 public:
  MainMenu() {
    Add(&container);
    container.Add(&left_menu);

    left_menu.entries = {
        L"Add project",
        L"Edit project",
        L"Remove project",
    };

    left_menu.on_enter = [this]() { on_enter(); };
  }

  std::function<void()> on_enter = []() {};

 private:
  Container container = Container::Horizontal();
  Menu left_menu;

  Element Render() override {
    int index = left_menu.selected;
    std::string descriptions[] = {"Create a new project.", "Edit a project.",
                                  "Remove a project"};
    return border(vbox({
        // -------- Top panel --------
        hbox({
            // -------- Left Menu --------
            vbox({
                hcenter(bold(text(L"SMAN: A software project manager"))),
                separator(),
                left_menu.Render(),
            }) | flex,

        }),
        separator(),
        // -------- Bottom panel --------
        vbox({
            hbox({
                text(L"  Description : "),
                text(to_wstring(descriptions[index])),
            }),
        }) | flex,
    }));
  }
};
