#include <sqlite3.h>
#include <stdlib.h>

#include "db/DBManager.cc"
#include "ftxui/component/screen_interactive.hpp"
#include "ui/MainMenu.cc"

using namespace ftxui;
using namespace std;

int main(int argc, const char *argv[]) {
  /* Instatiating the database */
  DBManager manager;
  manager.create_project_table();
  manager.create_todo_table();

  /* Starting up the UI */
  auto screen = ScreenInteractive::Fullscreen();
  MainMenu mainMenu;
  mainMenu.on_enter = screen.ExitLoopClosure();
  screen.Loop(&mainMenu);
}
