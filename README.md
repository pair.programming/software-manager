# Software Manager

## Cloning The Repository
> `git clone https://gitlab.com/pair.programming/software-manager`

## Requirements
- Cmake
- gcc
- sqlite3
- libfmt
- ftxui

## Running The Application
> UNIX
  - Generating Build Files
    - `cd software-manager`
    - `mkdir build;cd build`
    - `cmake ..`
    - `make`
  - Runing the Executables
    - `cd build`
    - `./sman`
